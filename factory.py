from components import (Collidable, Destructible, Model, Physics,
                        Player, Position, Stationary, WeaponManager)
from constants import AST_INIT_SCALE
from helpers import loadObject

def create_asteroid(base, manager, model, posX, posY, rot, velX, velY):
    components = {}
    components[Collidable.cname()] =  Collidable()
    components[Destructible.cname()]= Destructible()
    components[Model.cname()] =       Model(loadObject(base.loader, base.camera, model, scale = AST_INIT_SCALE))
    components[Position.cname()] =    Position(posX, posY, rot, velX, velY)
    manager.create_entity(components)

def create_ship(base, manager, posX, posY, rot, velX, velY):
    components = {}
    components[Collidable.cname()] =  Collidable()
    components[Player.cname()] =      Player()
    components[Destructible.cname()]= Destructible()
    components[Model.cname()] =       Model(loadObject(base.loader, base.camera, "ship"))
    components[Position.cname()] =    Position(posX, posY, rot, velX, velY)
    weapons = [{'dmg':1, 'rate':10, 'vel':10, 'ran':100, 'acc':1},
               {'dmg':10, 'rate':5, 'vel':10, 'ran':100, 'acc':0.5}]
    components[WeaponManager.cname()] = WeaponManager(weapons)
    manager.create_entity(components)

def create_bullet(base, manager, posX, posY, velX, velY):
    components = {}
    components[Collidable.cname()] =  Collidable()
    components[Model.cname()] =       Model(loadObject(base.loader, base.camera, "bullet", scale = 0.2))
    components[Position.cname()] =    Position(posX, posY, 0, velX, velY)
    manager.create_entity(components)

def create_starfield(base, manager, posX, posY, rot):
    components = {}
    components[Model.cname()] =       Model(loadObject(base.loader, base.camera, "stars", scale = 146, depth = 200, transparency = False))
    components[Position.cname()] =    Position(posX, posY, rot, 0, 0)
    components[Stationary.cname()] =  Stationary()
    manager.create_entity(components)
