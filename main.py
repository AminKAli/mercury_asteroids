from mercury.managers import PandaManager
from mercury.core.conditions import And
from systems import (AsteroidSystem, DestructionSystem, InputSystem,
                    PhysicsSystem, RenderSystem, WeaponsSystem)
from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText
from panda3d.bullet import BulletWorld
from panda3d.core import Vec3
from helpers import genLabelText
import factory


class Asteroids(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        asteroidsManager = PandaManager(100, self.taskMgr, "main_chain", 0, False, None, -1, False, False)
        world = BulletWorld()

        factory.create_starfield(self, asteroidsManager, 0, 0, 0)
        factory.create_ship(self, asteroidsManager, 0, 0, 0, 0, 0)
        factory.create_asteroid(self, asteroidsManager, "asteroid1", 50, 0, 0, 0, 0)
        factory.create_asteroid(self, asteroidsManager, "asteroid2", 0, 50, 0, 0, 0)
        factory.create_asteroid(self, asteroidsManager, "asteroid3", 50, 50, 0, 0, 0)
        factory.create_asteroid(self, asteroidsManager, "asteroid1", -50, -50, 0, 0, 0)

        self.title = OnscreenText(text="Panda3D: Tutorial - Tasks", style=1, fg=(1,1,0,1), pos=(0.8,-0.95), scale = .07)
        self.escapeText =   genLabelText("ESC: Quit", 0)
        self.leftkeyText =  genLabelText("[Left Arrow]: Turn Left (CCW)", 1)
        self.rightkeyText = genLabelText("[Right Arrow]: Turn Right (CW)", 2)
        self.upkeyText =    genLabelText("[Up Arrow]: Accelerate", 3)
        self.spacekeyText = genLabelText("[Space Bar]: Fire", 4)

        base.disableMouse()       # Disable default mouse-based camera control

        world.setGravity(Vec3(0, 0, -9.81))

        asteroidsManager.add_system(RenderSystem())
        asteroidsManager.add_system(DestructionSystem())
        asteroidsManager.add_system(InputSystem(self))
        asteroidsManager.add_system(AsteroidSystem())
        asteroidsManager.add_system(PhysicsSystem(world))
        asteroidsManager.add_system(WeaponsSystem())

game = Asteroids()
game.setFrameRateMeter(True)
game.run()