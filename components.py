from mercury import Component
from panda3d.core import Point2,Vec3

class Collidable(Component):
    def __init__(self):
        pass

class Player(Component):
    def __init__(self):
        pass

class Destructible(Component):
    def __init__(self, destroyed = False):
        self.destroyed = destroyed

class Model(Component):
    def __init__(self, model):
        self.obj = model

class Position(Component):
    def __init__(self, pX, pY, rot, vX, vY):
        self.pos = Point2(pX, pY)
        self.vel = Vec3(vX, 0, vY)
        self.rot = rot

class Stationary(Component):
    def __init__(self):
        pass

class WeaponManager(Component):
    def __init__(self, weapons):
        self.weapons = weapons
        self.current = weapons[0] if (len(weapons) > 0) else None
        self.fire = False

class Physics(Component):
    def __init__(self, body):
        self.body = body