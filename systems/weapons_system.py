from mercury import EntitySystem, conditions
from components import Model, Position

class WeaponsSystem(EntitySystem):

    condition = conditions.And('Model', 'Position', 'Player', 'WeaponManager')
    last = 0

    def tick(self, data):
        dt = data.time - self.last
        self.last = data.time
        super(WeaponsSystem, self).tick(dt)
        return data.cont

    def process(self, entity_id, components, dt):
        model = components['Model']
        Position = components['Position']
        WeaponManager = components['WeaponManager']

        if WeaponManager.fire:
            print "Fire:["+str(WeaponManager.current['dmg'])+"]"
            WeaponManager.fire = False

    def prepare(self):
        pass

    def update(self, dt):
        pass

    def destroy(self):
        pass