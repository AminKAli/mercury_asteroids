from mercury import VoidSystem

class AsteroidSystem(VoidSystem):  # Responsible for adding asteroids to the play field as the game progresses

    last = 0

    def tick(self, data):
        dt = data.time - self.last
        self.last = data.time
        super(AsteroidSystem, self).tick(dt)
        return data.cont

    def prepare(self):
        pass

    def update(self, dt):
        pass

    def destroy(self):
        pass