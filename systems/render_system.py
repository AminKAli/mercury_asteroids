from mercury import EntitySystem, conditions
from components import Model, Position

class RenderSystem(EntitySystem):  # Responsible for updating the rendered model object for all in game objects

    condition = conditions.And('Model', 'Position', conditions.AndNot('Stationary'))
    last = 0

    def tick(self, data):
        dt = data.time - self.last
        self.last = data.time
        super(RenderSystem, self).tick(dt)
        return data.cont

    def process(self, entity_id, components, dt):
        model = components[Model.cname()]
        position = components[Position.cname()]
        model.obj.setX(position.pos.getX())
        model.obj.setZ(position.pos.getY())
        model.obj.setR(position.rot)

    def prepare(self):
        pass

    def update(self, dt):
        pass

    def destroy(self):
        pass