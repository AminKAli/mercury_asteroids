from mercury import EntitySystem, conditions
from components import Model, Position

class PhysicsSystem(EntitySystem):

    condition = conditions.And('Model', 'Position', 'Physics')
    last = 0

    def __init__(self, world):
        super(PhysicsSystem, self).__init__()
        self.world = world

    def tick(self, data):
        dt = data.time - self.last
        self.last = data.time
        super(PhysicsSystem, self).tick(dt)
        return data.cont

    def process(self, entity_id, components, dt):
        model = components['Model']
        position = components['Position']
        physics = components['Physics']

    def prepare(self):
        pass

    def update(self, dt):
        self.world.doPhysics(dt)
        pass

    def destroy(self):
        pass