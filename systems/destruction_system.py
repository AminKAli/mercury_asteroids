from mercury import EntitySystem, conditions

class DestructionSystem(EntitySystem):  # Responsible for destroying/breaking asteroids as the player interacts with them

    condition = conditions.And('Position', 'Velocity', 'Destructible')
    last = 0

    def tick(self, data):
        dt = data.time - self.last
        self.last = data.time
        super(DestructionSystem, self).tick(dt)
        return data.cont

    def process(self, entity_id, components, dt):
        pass

    def prepare(self):
        pass

    def update(self, dt):
        pass

    def destroy(self):
        pass