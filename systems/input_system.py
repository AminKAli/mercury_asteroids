from mercury import EntitySystem, conditions
from components import Position, Model
from constants import TURN_RATE, DEG_TO_RAD, ACCELERATION, MAX_VEL_SQ, MAX_VEL
from panda3d.core import Point2,Vec3
from math import sin, cos, pi
import sys

class InputSystem(EntitySystem):  # Responsible for taking the user's input and applying it to their character

    condition = conditions.And('Position', 'Model', 'Player', 'WeaponManager')
    base = None
    last = 0
    keys = {"turnLeft" : 0, "turnRight": 0, "accel": 0, "fire": 0}

    def __init__(self, base):
        super(InputSystem, self).__init__()
        self.base = base

    def tick(self, data):
        dt = data.time - self.last
        self.last = data.time
        super(InputSystem, self).tick(dt)
        return data.cont

    def process(self, entity_id, components, dt):
        model = components['Model']
        position = components['Position']
        weaponManager = components['WeaponManager']

        rot_change = 0
        if self.keys["turnRight"]:
            rot_change += dt * TURN_RATE
        elif self.keys["turnLeft"]:
            rot_change -= dt * TURN_RATE
        position.rot  = (position.rot + rot_change) % 360

        if self.keys["accel"]:
            rot_rad = DEG_TO_RAD * position.rot
            newVel = (Vec3(sin(rot_rad), 0, cos(rot_rad)) * ACCELERATION * dt)
            newVel += position.vel
            #Clamps the new velocity to the maximum speed. lengthSquared() is used
            #again since it is faster than length()
            if newVel.lengthSquared() > MAX_VEL_SQ:
                newVel.normalize()
                newVel *= MAX_VEL
            position.vel = newVel

        if self.keys["fire"]:
            weaponManager.fire = True


    def prepare(self):
        self.base.accept("escape", sys.exit)  # Escape quits
        self.base.accept("arrow_left",     self.setKey, ["turnLeft", 1])
        self.base.accept("arrow_left-up",  self.setKey, ["turnLeft", 0])
        self.base.accept("arrow_right",    self.setKey, ["turnRight", 1])
        self.base.accept("arrow_right-up", self.setKey, ["turnRight", 0])
        self.base.accept("arrow_up",       self.setKey, ["accel", 1])
        self.base.accept("arrow_up-up",    self.setKey, ["accel", 0])
        self.base.accept("space",          self.setKey, ["fire", 1])
        self.base.accept("space-up",          self.setKey, ["fire", 0])

    def update(self, dt):
        pass

    def destroy(self):
        pass

    def setKey(self, key, val):
        self.keys[key] = val
